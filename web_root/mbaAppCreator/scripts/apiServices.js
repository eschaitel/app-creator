define(['angular'], function(angular) {
// Operator	Description
// ==	Equals
// =gt=	Greater Than
// =ge=	Greater Than or Equal
// =lt=	Less Than
// =le=	Less Than or Equal
// ;	AND Boolean (to be used only between expressions)
// *	Wildcard for use for string queries. Can only be used at the end of the string value.
	
	angular.module("psApiServices",["dateService"])
	.service("apiService",function($http, $q, dateService){
		var service = this;
		service.apiPath = '/ws/schema/table/';
		service.pqPath = '/ws/schema/query/';
		service.tables = {};
		service.headers = {
			'Accept': 'application/json', 
			"Content-Type": "application/json",
			"X-Mba-Plugin": "MBA Application Creator"
		};
		service.pageSize = 100;
		//MBA relay: 'https://psrelay.mba-link.com

		this.setDateFormat = function(format){
			dateService.setDateFormat(format);
		};

		this.useRelay = function(){
			service.apiPath = 'https://psrelay.mba-link.com/ws/schema/table/';
			service.pqPath = 'https://psrelay.mba-link.com/ws/schema/query/';
		}
		
		/****************************  GET RECORDS  ****************************/
		this.getRecords = function(tbl,sortVal, criteria, parentData){
			tbl = tbl.toLowerCase();
			var deferredResponse = $q.defer();
			var path = service.apiPath;

			service.loadTableDef(tbl).then(function(){
				//get count of records for pagination
				var pageCount = 1;
				$http({
					"url": path + tbl + '/count', 
					"method": "GET",
					"params":{"q":criteria},
					"headers": service.headers
				}).then(function(response){
					if(response.data.count == 0){
						deferredResponse.resolve([]);
					}else{
						pageCount = Math.ceil(response.data.count/service.pageSize); 
						var records = [];
						var completedRequests = 0;
						for(var i = 1; i <= pageCount; i++){
							service.getPage(tbl, i, sortVal, criteria, parentData).then(function(response){
								completedRequests ++;
								records = records.concat(response);
								if(completedRequests == pageCount) deferredResponse.resolve(records);
							});
						}
					}
				});
			});
			return deferredResponse.promise;         
		};

		this.getPage = function(tbl, pageNum, sortVal, criteria, parentData){
			tbl = tbl.toLowerCase();
			var deferredResponse = $q.defer();
			var path = service.apiPath;
			var payload = {
				"projection": "*",
				"page": pageNum,
				"pagesize": service.pageSize,
				"sort":sortVal,
				"q": criteria
			};
			var dates = service.tables[tbl].dateFields;
			var booleans = service.tables[tbl].booleanFields;
			var records;
			var currentRecord;
			var simpleRecords = [];
			$http({
				"url": path + tbl, 
				"method": "GET",
				"params":payload,
				"headers": service.headers
			}).then(function(response){
				records = response.data.record;
				angular.forEach(records,function(record){
					currentRecord = record.tables[tbl];
					angular.forEach(booleans,function(fieldName){
						if(angular.isDefined(currentRecord[fieldName])){
							if(currentRecord[fieldName] == 'true') currentRecord[fieldName] = true;
							else currentRecord[fieldName] = false;
						}
					});
					angular.forEach(dates,function(fieldName){
						if(angular.isDefined(currentRecord[fieldName])){
							currentRecord[fieldName] = dateService.formatDateFromApi(currentRecord[fieldName]);
						}
					});
					angular.forEach(parentData,function(dataRequest){
						currentRecord[dataRequest.storeAs] = record.tables[dataRequest.table][dataRequest.field];
					});
					simpleRecords.push(currentRecord);
				});
				deferredResponse.resolve(simpleRecords);
			});
			return deferredResponse.promise;
		};

		/****************************	ADD/MODIFY RECORD	****************************/
		this.addModifyRecord = function(tbl, recordData){
			tbl = tbl.toLowerCase();
			var deferredResponse = $q.defer();
			var recId = 0;
			var recordCopy = angular.copy(recordData);
			
			//must remove id from object prior to updating record
			if(angular.isDefined(recordCopy.id)){
				recId = recordCopy.id;
				delete recordCopy.id;
			}

			service.loadTableDef(tbl).then(function(){
				var dates = service.tables[tbl].dateFields;
				var booleans = service.tables[tbl].booleanFields;

				//convert booleans and dates to string formats accepted by API
				angular.forEach(booleans,function(fieldName){
					if(angular.isDefined(recordCopy[fieldName])){
						if(typeof recordCopy[fieldName] == 'string') 
							recordCopy[fieldName] = recordCopy[fieldName].toLowerCase();
						if(recordCopy[fieldName] === 'false' || recordCopy[fieldName] === '0') 
							recordCopy[fieldName] = false;
						recordCopy[fieldName] = recordCopy[fieldName] ? 'true' : 'false';
					}
				});
				angular.forEach(dates,function(fieldName){
					if(angular.isDefined(recordCopy[fieldName])){
						recordCopy[fieldName] = dateService.formatDateForApi(recordCopy[fieldName]);
					}
				});

				//make sure everything is a string before submitting to API
				for(var key in recordCopy){
					recordCopy[key] = recordCopy[key].toString();
					if(service.tables[tbl].fieldNames.indexOf(key.toLowerCase()) < 0) delete recordCopy[key];
				}

				// An id will be passed if updating an existing record.
				// The url and method must be set accordingly.
				var url = service.apiPath + tbl + ( recId ? '/' + recId : '');
				var payload = {"tables": {}};
				payload.tables[tbl] = recordCopy;

				$http({
					"url": url,
					"method": recId ? 'PUT' : 'POST',
					"data": payload,
					"headers": service.headers
				}).then(function(response){
					var result = response.data.result[0];
					if(result.status == 'SUCCESS'){
						deferredResponse.resolve(result.success_message.id);
					}else{
						var messages = [];
						angular.forEach(result.error_message.error,function(err){
							messages.push(err.error_description);
						});
						deferredResponse.resolve(messages);
					}
				},function(res){
					console.log("err: ", res);
				}); 
			});
			return deferredResponse.promise;         
		};

		/****************************	DELETE RECORD	****************************/
		this.deleteRecord = function(tbl, id){
			tbl = tbl.toLowerCase();
			var deferredResponse = $q.defer();
			$http.delete(service.apiPath + tbl + '/' + id, {"headers": service.headers}).then(function(response){
				deferredResponse.resolve(response);
			});
			return deferredResponse.promise; 
		};

		/****************************	GET POWERQUERY	****************************/
		this.getPQResults = function(query, data, currentStudents){
			var deferredResponse = $q.defer();
			var parameters = {"pagesize":0};
			if(currentStudents) parameters.dofor = "selection:selectedstudents";
			 $http({
				url: service.pqPath + query,
				method:'POST',
				data: data,
				params: parameters,
				headers: service.headers
			}).then(function(response){
				deferredResponse.resolve(response.data.record || []);
			},function(res){
				console.log(res.data.message);
			});
			return deferredResponse.promise;
		};

		/****************************	GET TABLE DEFINITION	****************************/
		this.loadTableDef = function(tbl){
			tbl = tbl.toLowerCase();
			var deferredResponse = $q.defer();

			if(service.tables[tbl]){
				deferredResponse.resolve();
			}else{
				var fieldNames = [];
				var booleans = [];
				var dates = [];
				service.getPQResults('com.mba.mbaAppCreator.tables.tableDefinition',{"tableName":tbl}).then(function(fields){
					angular.forEach(fields,function(field){
						fieldNames.push(field.name.toLowerCase());
						if(field.type == "Boolean") booleans.push(field.name.toLowerCase());
						else if(field.type == "Date") dates.push(field.name.toLowerCase());
					});
					service.tables[tbl] = {	"fieldNames": fieldNames, "booleanFields": booleans, "dateFields": dates };
					deferredResponse.resolve();
				});
			}
			return deferredResponse.promise;
		};
	});
});