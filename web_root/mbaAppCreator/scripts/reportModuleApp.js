define(['/mbaAppCreator/scripts/apiServices.js?_v=2', '/mbaAppCreator/scripts/dateService.js'], function() {
	var moduleApp = angular.module('moduleApp', ['ngSanitize','dateService','psApiServices']);
	moduleApp.controller('moduleCtrl', function($scope, $http, $q, $timeout, $sce, apiService, dateService) {
		if(mba_ps_dateFormat) dateService.setDateFormat(mba_ps_dateFormat);
		$scope.schools = mba_ps_schools;
		$scope.years = mba_ps_years;
		$scope.coreTable = 'Students';
		$scope.modules = {};
		var modUrl = '/admin/mbaAppCreator/studentModules/appModules.json';
		if(mba_ps_portal == 'teacher') modUrl = '/teachers/mbaAppCreator/dataAccess/appModules.json';
		if(['student','guardian'].includes(mba_ps_portal)) modUrl = '/guardian/mbaAppCreator/dataAccess/appModules.json';
		if(mba_ps_portal != 'admin') apiService.useRelay();
		apiService.loadTableDef('u_mba_app_records');

		$http({
			"url": modUrl,
			"method": 'POST',
			"data": $j.param({"association":"App Creator"}),
			"headers" : {"Content-Type": "application/x-www-form-urlencoded" }
		}).then(function(response){
			let res = response.data;
			delete res.rem;
			for(key in res){
				let mod = res[key];
				if(!mod[mba_ps_portal + '_access']) continue;
				mod.cat = mod.cat.replaceAll('&amp;','&');
				mod.fields = mod.fields.replaceAll('&amp;','&');
				mod.fields = JSON.parse(mod.fields);
				mod.permissions = JSON.parse(mod.permissions);
				mod.schools = JSON.parse(mod.schools);
				mod.permissions = mod.permissions[mba_ps_portal];
				mod.booleanFields = [];
				mod.numericFields = [];
				mod.integerFields = [];
				mod.dateFields = ['whencreated','whenmodified'];
				mod.primarySort = 'whencreated';
				mod.secondarySort = 'whencreated';
				angular.forEach(mod.fields,function(fld){
					if(['Integer','Double'].includes(fld.dataType)) mod.numericFields.push(fld.field);
					if(fld.dataType == 'Integer') mod.integerFields.push(fld.field);
					if(fld.dataType == 'Boolean') mod.booleanFields.push(fld.field);
					if(fld.dataType == 'Date') mod.dateFields.push(fld.field);
					if(fld.field == 'schoolid') mod.trackSchool = true;
					if(fld.field == 'yearid') mod.trackYear = true;
					if(fld.primarySort) mod.primarySort = fld.field; 
					if(fld.secondarySort) mod.secondarySort = fld.field; 
				});
				if(mod.schools.length == 0 || mod.schools.includes(mba_ps_curSchool) || mba_ps_curSchool == '0'){
					if(mba_ps_portal == 'admin'){
						let groupSetting = checkModSecurityAccess(mod);
						if(groupSetting == 'full') $scope.modules[key] = mod;
						if(groupSetting == 'readonly'){
							mod.permissions.create = false;
							mod.permissions.update = false;
							mod.permissions.delete = false;
							$scope.modules[key] = mod;
						}
					}else if(mba_ps_portal == 'teacher'){
						//show this module only if allowed for all students or if current student
						//is in one of the teacher's students
						if(mod.permissions.allStudents || mba_teachers_student) $scope.modules[key] = mod;
					}
					else $scope.modules[key] = mod;
				}
			}
			if(Object.keys($scope.modules).length){
				$scope.selectedModule = $scope.modules[Object.keys($scope.modules)[0]];
			}else{
				$scope.noModsAvailable = true;
			}

			$scope.multipleModules = Object.keys($scope.modules).length > 1;
			$j('#loadingDiv').remove();
		}); //End module retrieval

		$scope.editRecord = {};

		function checkModSecurityAccess(mod){
			let response = "none";
			mba_ps_roles.forEach(function(role){
				if(mod.permissions.groupAccess[role] == 'full') response = 'full';
				else if(mod.permissions.groupAccess[role] == 'readonly' && response == 'none')
					response = 'readonly';
			});
			return response;
		}

		$scope.allowNewRecord = function(){
			if(!$scope.currentRecords || !$scope.selectedModule) return false;
			let curCount = Object.keys($scope.currentRecords).length;
			if($scope.selectedModule.single_record && curCount > 0) return false;
			return $scope.selectedModule.permissions.create;
		};

		$scope.newRecord = function(){
			$scope.editRecord = {};
			if($scope.selectedModule.trackSchool) $scope.editRecord.schoolid = mba_ps_curSchool;
			if($scope.selectedModule.trackYear) $scope.editRecord.yearid = mba_ps_curYear;
			$j('#updateRecordDialog').show(500);
		};

		$scope.markForUpdate = function(rec){
			$scope.editRecord = angular.copy(rec);
			$j('#updateRecordDialog').show(500);
		};

		$scope.submitUpdate = function(){
			var editForm = $j('form[name="editRecordForm"]');
			editForm.addClass('submitted');
			if($scope.editRecordForm.$valid && editForm.find('.psDateWidget.error').length == 0){
				$scope.editRecord.u_mba_app_modulesID = $scope.selectedModule.id;
				$scope.editRecord.core_table = $scope.coreTable;
				if($scope.editRecord.id){
					delete $scope.editRecord.u_mba_app_modulesID;
				}else{
					$scope.editRecord.core_table_dcid = mba_ps_curDcid;
				}
				//check integer values
				$scope.selectedModule.integerFields.forEach(function(fld){
					if($scope.editRecord[fld]){
						$scope.editRecord[fld] = parseInt($scope.editRecord[fld]);
						if(isNaN($scope.editRecord[fld])) $scope.editRecord[fld] = '0';
					}
				});
				apiService.addModifyRecord('u_mba_app_records', $scope.editRecord).then(function(res){
					if(isNaN(res)){
						psAlert({message:res.toString(),title:'Unable to save record'});
						return;
					}
					$scope.editRecord.id = res;
					$scope.currentRecords[res] = angular.copy($scope.editRecord);
					$scope.closeRightDialog();
					$scope.currentRecords[res].sort_val = getSortVal($scope.currentRecords[res]);
				});
			}
		};

		$scope.getDisplayVal = function(rec, field){
			if(!rec[field.field]) return '';
			else if(field.field == 'schoolid') return $scope.schools[rec[field.field]].name;
			else if(field.field == 'yearid') return $scope.years[rec[field.field]].abbreviation;
			return rec[field.field];
		};

		$scope.currentRecords = {};

		$scope.$watch('selectedModule',function(){
			if($scope.selectedModule && $scope.selectedModule.id) getRecords();
		});

		$scope.closeRightDialog = function(){
			$j('.dialogRight').hide(500);
			$j('form[name="editRecordForm"]').removeClass('submitted');
		};

		$scope.deleteRec = function($event, rec){
			psConfirm({
				title:"Confirm Delete",
				message:"Please confirm deletion of this record",
				oktext:"Confirm",
				canceltext:"Cancel",
				ok: function(){
					apiService.deleteRecord('u_mba_app_records',rec.id).then(function(res){
						$j($event.currentTarget).closest('tr').hide(700);
						$timeout(function(){
							delete $scope.currentRecords[rec.id];
						},700);
					});
				}
			});
		};

		$scope.requiredError = function(fld){
			return (fld.required && $j('form[name="editRecordForm"]').hasClass('submitted') 
				&& !$scope.editRecord[fld.field]);
		};

		function getRecords(){
			$scope.showTotals = false;
			let url = '/admin/mbaAppCreator/studentModules/appModuleRecords.json';
			if(mba_ps_portal == 'teacher') 
				url = '/teachers/mbaAppCreator/dataAccess/appModuleRecords.json';
			else if(['student','guardian'].includes(mba_ps_portal)) 
				url = '/guardian/mbaAppCreator/dataAccess/appModuleRecords.json';
			let params = {
				"coretable":$scope.coreTable,
				"coredcid":mba_ps_curDcid,
				"categoryid": $scope.selectedModule.id
			};
			$http({
				"url": url,
				"method": 'POST',
				"data": $j.param(params),
				"headers" : {"Content-Type": "application/x-www-form-urlencoded" }
			})
			.then(function(response){
				delete response.data.rem;
				$scope.currentRecords = response.data;
				var numFields = $scope.selectedModule.numericFields;
				var booleanFields = $scope.selectedModule.booleanFields;
				angular.forEach($scope.currentRecords,function(rec){
					for(key in rec){
						if(numFields.includes(key) || key == 'yearid') rec[key] = Number(rec[key]);
						else if(booleanFields.includes(key)) rec[key] = rec[key] == '1';
						else if(typeof rec[key] == 'string') rec[key] = rec[key].replaceAll('&amp;','&');
					}
					rec.sort_val = getSortVal(rec);
				});
				$scope.recCount = Object.keys($scope.currentRecords).length;
			});
		}//End getRecords()

		$scope.convertNum = (val) => Number(val);

		function getSortVal(rec){
			let mod = $scope.selectedModule;
			let prim = rec[mod.primarySort];
			let sec = rec[mod.secondarySort];
			if(mod.dateFields.includes(mod.primarySort)) prim = dateService.formatDateForApi(prim);
			if(mod.dateFields.includes(mod.secondarySort)) sec = dateService.formatDateForApi(sec);
			return prim + '' + sec;
		};

		$scope.getTotal = function(field){
			if(!Object.keys($scope.currentRecords).length) return '';
			if(field.indexOf('int') != 0 && field.indexOf('double') != 0) return '';
			let total = 0;
			let decimals = 0;
			angular.forEach($scope.currentRecords,function(rec){
				let val = rec[field];
				if(!val) return;
				let parts = val.toString().split('.');
				if(parts.length > 1 && parts[1].length > decimals) decimals = parts[1].length;
				total += val;
			});
			return total.toFixed(decimals);
		};

		$scope.trunc = function(val, length){
			if(val.length <= length) return val;
			else return val.substring(0,length) + '...';
		}
	}); //End Controller

	moduleApp.directive("editRecord", function (){
		return {
			restrict: 'AEC', //Attribute, Element, Class
			templateUrl: '/mbaAppCreator/editModuleRecord.html',
			scope:false,
			link: function(scope){
				//initialize date widgets
				let observer = new MutationObserver(function(){
					if($j('#editRecTable .psDateWidget').length) initBehaviors();
				});
				var targetNode = $j('#editRecTable')[0];
				observer.observe(targetNode, { childList: true, subtree: true });
			}
		}
	});

	moduleApp.filter('orderObjectBy', function() {
	    return function(items, field, reverse) {
	        var filtered = [];
	        angular.forEach(items, item => filtered.push(item) );
	        filtered.sort((a, b) => ((a[field] || '') > (b[field] || '') ? 1 : -1) );
	        if(reverse) return filtered.reverse();
	        else return filtered;
	    };            
	});
});