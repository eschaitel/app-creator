<div class="hide">
	<li id="appCreatorLink">
		<a href="/admin/mbaAppCreator/home.html">MBA App Creator</a>
	</li>
</div>
<script type="text/javascript">
$j(document).ready(function(){	
	let linkTxt = `~[displaypref:appCLeftNavTxt]`;
	if(linkTxt.trim()) $j('#appCreatorLink a').text(linkTxt);
	appendLink();
	function appendLink(link){
		var mbaList;
		if($j('ul#mbaPlugins').length > 0){
			mbaList = $j('ul#mbaPlugins');
		}else{
			mbaList = $j('<ul id="mbaPlugins">');
			if($j('#left_nav_setup').length) $j('#left_nav_setup').after(mbaList);
			else $j('#nav-main-menu').append(mbaList);
			mbaList.before("<H2>MBA Plugins</H2>");
		}
		mbaList.append($j('#appCreatorLink'));

		//sort plugin list
		var items = $j('ul#mbaPlugins > li').get();
		items.sort((a,b) => a.textContent.trim() <= b.textContent.trim() ? -1 : 1);
		items.forEach(e => $j('ul#mbaPlugins').append(e));
	}
});
</script>